import { useEffect , useContext } from 'react'
import { getBooks } from '../services/index'
import booksContext from '../context/Books'
const useGetBooks = () => {
    const { setBooks , setFilteredBooks} = useContext(booksContext)
    const setBooksMethod = async () => {
        const data = await getBooks()
        setBooks( data )
        setFilteredBooks( data )
    } 
    useEffect( () => {  
        setBooksMethod()        
    } , [] ) // eslint-disable-line react-hooks/exhaustive-deps
}
export default useGetBooks