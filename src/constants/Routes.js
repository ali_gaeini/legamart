import Login from "../pages/Login"
import Books from "../pages/Books"
import BookDetails from "../pages/BookDetails"
import NotFound from '../components/NotFound'
const routes = [
    {
        path : '/' , 
        component : Login
    } , 
    {
        path : '/book' , 
        component : Books
    } ,
    {
        path : '/detail' , 
        component : BookDetails
    } , 
    {
        path : '*' , 
        component : NotFound
    } 
]
export default routes
