import Toast from '../components/Toast'
const axios = require('axios');
const instance = axios.create({
    baseURL: 'http://localhost:9000',
    headers: {
      'Content-Type' : 'application/json'
  }
  });
  const responseHandler = response => {
    return response;
  };
  const errorHandler = error => {
    Toast( error.message, "white" , "red" )
    return Promise.reject(error);
  };
  instance.interceptors.response.use(
    (response) => responseHandler(response),
    (error) => errorHandler(error)
 );
export default  instance 