import React, { useState , useContext } from 'react';
import userContext from '../context/User';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { login } from '../services'
import { Redirect } from 'react-router-dom';
const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const Login = () =>  {
  const classes = useStyles();
  const UserContext = useContext(userContext)
  const [ username , setUsername ] = useState("")
  const [ password , setPassword ] = useState("")
  const [ redirect , setRedirect ] = useState(false) 
  const loginMethod = async () => {
    const data = await login( username , password )
    if( data && data.id ){
      localStorage.setItem( "login" , true )
      setRedirect( true )
      UserContext.setUser( data )
    } 
  }
  if( redirect ){
    return <Redirect to = "/book"/>
  }else{
    return (
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <form className={classes.form} noValidate>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              label="username"
              value={username}
              onChange={ e => setUsername( e.target.value ) }
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              label="Password"
              type="password"
              value={ password }
              onChange={ e => setPassword( e.target.value ) }
            />
            <Button
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              onClick = { () =>loginMethod() }
            >
              Sign In
            </Button>
          </form>
        </div>
      </Container>
    );
  }
  
}
export default Login