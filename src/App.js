import './assets/css/App.css'
import Routes from './routes/index'
import userContext from './context/User'
import booksContext from "./context/Books";
import bookDetailContext from "./context/BookDetail";
import { useState } from "react";
const App = () => {
  const [ user , setUser ] = useState({})
  const [ books , setBooks ] = useState([])
  const [ filteredBooks , setFilteredBooks ] = useState([])
  const [ bookDetail , setBookDetail ] = useState({})
  const [ ShowType ,setShowType ] = useState("Table")
  return ( 
    <userContext.Provider value={{ user , setUser }}>
      <booksContext.Provider value={{ books, filteredBooks , ShowType , setBooks , setFilteredBooks , setShowType }}>
        <bookDetailContext.Provider value={{ bookDetail ,setBookDetail }}>
            <Routes />
        </bookDetailContext.Provider>
      </booksContext.Provider> 
    </userContext.Provider>
  )
}

export default App;
