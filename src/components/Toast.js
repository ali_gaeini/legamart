import Toastify from 'toastify-js'
import "toastify-js/src/toastify.css"
const Toast = ( text , color , backgroundColor) => {
     Toastify({
        text: text,
        backgroundColor: backgroundColor,
        color : color , 
        gravity: "bottom",
        position: "left",
      }).showToast()
}
export default Toast