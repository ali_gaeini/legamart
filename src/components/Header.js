import React , { useContext  , useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import userContext from '../context/User';
import { Redirect } from 'react-router-dom';
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));
const Header = () =>  {
  const classes = useStyles();
  const [ redirect , setRedirect ] = useState( false )
  const { user } = useContext(userContext)
  const logOut = () => {
      localStorage.removeItem("login")
      setRedirect( true )
  }
  if( redirect ){
    return <Redirect to="/"/>
  }else{
    return (
        <div className={classes.root}>
          <AppBar position="static">
            <Toolbar>
              <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu" onClick={ logOut }>
                <ExitToAppIcon />
              </IconButton>
              <Typography variant="h6" className={classes.title}>
                LegaMart
              </Typography>
              <Typography color="inherit">{user.fullName}</Typography>
            </Toolbar>
          </AppBar>
        </div>
          );
  }
}
export default Header
