import axios from '../constants/Axios'
const getBooks = async () => {
    let { data } = await axios.get('/books')
    return data.payload
}
const login = async ( username , password ) => {
    let { data } = await axios.post("/auth/login" , { username , password })
    return data.payload
}
export { getBooks , login }